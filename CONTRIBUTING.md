# Style Guidelines

## Code Blocks
In languages that use non-whitespace characters to denote code blocks,
the opening and closing character should be on on a newline, by t.

---


## JavaScript/TypeScript

### Functions
Function statements only.

### Statements
All statements should end with a semicolon, where possible.

---


## C/C++

### Using Namespace
```using namespace``` statements are never allowed.

---


## Python

### Be Pythonic
Be Pythonic, where possible. To new Python devs, this essentially means
leveraging the unique and powererful tools that are available in Python.
This also usually has an added benefit of tersing the code quite a bit.

### PEP8
Try to follow PEP8 guidelines as much as possible, but be reasonable and
don't be afraid to ignore the guidelines if it makes sense.

---


## File Names
All file names except Python must use camelcase. Python files must be
snakecase.

## Directory Names
each-word-of-dirname

---


## More Style Guidelines
These are not the only style guidelines. More will be added,
and are in active effect.

### To Add:
* Directory structure
* Filenames
* Variable names
* Line spacing
* More/etc.

---


## License

See [README](README.md) and [LICENSE](LICENSE).

---
