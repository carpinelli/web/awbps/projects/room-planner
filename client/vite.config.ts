import { defineConfig } from "vite"
import react from "@vitejs/plugin-react"

// https://vitejs.dev/config/
export default defineConfig(
  {
    plugins: [react()],
    base: "/web/awbps/projects/room-planner/",
    server:
    {
      host: true,
      port: 8082, // This is the port used in Docker.
      watch:
      {
        usePolling: true,
      },
      proxy:
      {
        "/api": "http://localhost:8081",
        "/auth": "http://localhost:8081",
      },
    },
  }
);
