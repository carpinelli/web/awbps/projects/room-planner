import React from "react";
import type { ReactNode } from "react";
import { Route, Routes } from "react-router-dom";

import PageLayout from "./pages/PageLayout";
import Home from "./pages/Home";
// import RoomPlanner from "./pages/RoomPlanner";
import About from "./pages/About";


const App = function()
{
  const home = (<PageLayout><Home /></PageLayout>);
  // const login = (<PageLayout><Login /></PageLayout>);
  // const signup = (<PageLayout><Signup /></PageLayout>);
  // const roomPlanner = (<PageLayout><roomPlanner /></PageLayout>);
  const about = (<PageLayout><About /></PageLayout>);
  const appJsx: ReactNode = (
    <main className="app">
      <Routes>
        <Route path="/" element={home} />
        {/* <Route path="/room-planner" element={roomPlanner} /> */}
        <Route path="/about" element={about} />
      </Routes>
    </main>
  );

  return (
    <>
      {appJsx}
    </>
  );
};

export default App;

