const Header = function({ text })
{
    return (
        <section className="header component">
            <h1>{text}</h1>
            <br />
        </section>
    );
};


export default Header;
