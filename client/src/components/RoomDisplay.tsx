import capitalCase from "../../utilities/capitalize";


const RoomDisplay = function({ room, additionalClasses = "" })
{
    return (!room.name
        ? <p className={["component", additionalClasses].join(" ")}>"Loading..."</p>
        : (
        <div className={["component", additionalClasses].join(" ")}>
            <h2>{capitalCase(room.name)}</h2>
            <hr /><br />
            <TypesDisplay types={pokemon.types} />
            <StatsDisplay stats={pokemon.stats} />
            <AbilitiesDisplay abilities={pokemon.abilities}/>
            <MiscellaneousDisplay pokemon={pokemon} />
            <br /><hr /><br />
            <MovesDisplay moves={pokemon.moves} />
            <GamesDisplay game_indices={pokemon.game_indices} />
            <br /><hr /><br />
            <FormsDisplay forms={pokemon.forms} />
            <SpeciesDisplay species={pokemon.species} />
            <br /><hr /><br />
            <SpritesDisplay sprites={pokemon.sprites} />
            <br /><hr /><br />
        </div>
        )
    );
};


export default PokeDisplay
