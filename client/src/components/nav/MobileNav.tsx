import React from "react";
import { useRef } from "react";

import NavItem from "./NavItem";


const MobileNav = function()
{
  // const mobileMenuRef = useRef<HTMLElement | null>(null);
  const mobileMenuRef = useRef<any>(null);
  const tooltipFrom: string = "tooltip-left";
  const loggedIn: boolean = true;
  const loginNav = (<NavItem linkpath="/login" text="Login" tooltipFrom={tooltipFrom} />);
  const logoutNav = (<NavItem linkpath="/logout" text="Logout" tooltipFrom={tooltipFrom} />);

  const handleClick = function(_: React.MouseEvent<HTMLDivElement, MouseEvent>)
  {
    console.log(_);
    if (mobileMenuRef.current)
    {
      mobileMenuRef.current.classList.toggle("hidden");
    }
  };

  
  return (
    <div onClick={handleClick} className="nav-mobile">
      <div className="hidden mobile-menu" ref={mobileMenuRef}>
        <NavItem linkpath="/rooms" text="Rooms" />
        <NavItem linkpath="/furniture" text="Furniture" />
        <NavItem linkpath="/layouts" text="Layouts" />
        <NavItem linkpath="/about" text="About" tooltipFrom={tooltipFrom} />
        {loggedIn ? logoutNav : loginNav}
      </div>
      {/* Get Hamburger Menu Icon */}
      <svg xmlns="https://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
        stroke="currentColor" className="hamburger-icon">
        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
          d="M4 6h16M4 12h16M4 18h16" />
      </svg>
    </div>
  );
};


export default MobileNav;

