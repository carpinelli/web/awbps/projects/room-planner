import React from "react";
import NavItem from "./NavItem";


const FullNav = function()
{
  const loggedIn: boolean = true;
  const loginNav = (<NavItem linkpath="/login" text="Login" />);
  const logoutNav = (<NavItem linkpath="/logout" text="Logout" />);

  return (
    <div className="nav-full">
      <div className="site-sections">
        <NavItem linkpath="/rooms" text="Rooms" />
        <NavItem linkpath="/furniture" text="Furniture" />
        <NavItem linkpath="/layouts" text="Layouts" />
      </div>
      <div className="about-section">
        <NavItem linkpath="/about" text="About" />
      </div>
      <div className="auth-section">
        {loggedIn ? logoutNav : loginNav}
      </div>
    </div>
  );
};


export default FullNav;
