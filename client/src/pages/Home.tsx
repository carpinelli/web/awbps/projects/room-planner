import React from "react";

import PageHeader from "../components/PageHeader";


const Home = function()
{
  return (
    <section className="page">
      <PageHeader text="Home Page" />
      <section>
        <p className="text-xl">Welcome to Room Planner!</p>
        <hr />
        <p>A MERN stack application for planning the rearrangement of rooms before
           having to move anything physically. Some intended features are to store
          and save rooms, furniture, and previously created layouts.</p>

        <p>This project can be used by anyone who wants to plan the layout of a
           room before moving anything. Instead of measuring furniture and walls
           as you go, you can enter all the measurements ahead of time and use the
           draggable elements to plan the room layout.</p>
        <br /><br />
      </section>
      <section>
        <p className="text-xl">Current Features</p>
        <hr /><br />
        <p className="text-lg">Rooms</p>
        <p>Lorem ipsum.</p>
        <br />
        <p className="text-lg">Furniture</p>
        <p>Currently under maintenance.</p>
        <br />
        <p className="text-lg">Layouts</p>
        <p>Currently under maintenance.</p>
        <br />
      </section>
    </section>
  );
};



export default Home;
