import React = require("react");

import type { Room } from "../../models/roomModel";


const Show = function({ room }: { room: Room })
{
  const styleSpacing = { margin: "1rem", };
  const styleBold = { fontWeight: "bold", };
  return (
    <div style={styleSpacing}>
      <h1>Room:</h1>
      <hr />
      <span>
        <span style={styleBold}>Name: </span>
        {room.name}
      </span>
      <p>
        <span style={styleBold}>Walls : </span>
        {room.walls}
      </p>
      <p>
        <span style={styleBold}>Doors: </span>
        {room.doors}
      </p>
      <hr />
      <a href="/rooms">Rooms</a>
      <hr />
    </div>
  );
};


export default Show;

