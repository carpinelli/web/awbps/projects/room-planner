import type { ReactElement } from "react";
import React = require("react");

import type { Room } from "../../models/roomModel";


const New = function(): ReactElement
{
  const styleSpacing = { margin: "1rem", };
  const styleBold = { fontWeight: "bold", };
  return (
    <div style={styleSpacing}>
      <p><span style={styleBold}>Add Room: </span></p>
      <form action="/rooms" method="POST">
        <ul style={{ listStyle: "none", margin: "0", padding: "0", }}>
          <li>
            <label htmlFor="name">Name: </label>
            <input type="text" id="name" name="name" />
          </li>
          <li>
            <label htmlFor="walls">Walls: </label>
            <input type="number" id="walls" name="walls" />
          </li>
          <li>
            <label htmlFor="doors">Doors: </label>
            <input type="number" id="doors" name="doors" />
          </li>
        </ul>
      </form>
    </div>
  );
};


export default New;

