import type { ReactElement } from "react";
import React = require("react");

import type { Room } from "../../models/roomModel";


interface RoomArray
{
  rooms: Room[];
}

const Index = function({ rooms }: RoomArray): ReactElement
{
  const styleSpacing = { margin: "1rem", };
  const styleBold = { fontWeight: "bold", };
  return (
    <div style={styleSpacing}>
      <h1>Rooms:</h1>
      {rooms.map((room) => (
        <div key={room.name}>
          <hr />
          <span>
            <span style={styleBold}>Name: </span>
            {room.name}
          </span>
          <span>
            <span style={styleBold}>Walls: </span>
            {room.walls}
          </span>
          <p>
            <span style={styleBold}>Doors: </span>
            {String(room.doors) + "    "}
            <a href={`/rooms/${room._id}`}>Details</a>
          </p>
        </div>
      ))}
      <hr />
      <a href="/rooms/New"><button>New</button></a>
      <hr />
    </div>
  );
};


export default Index;

