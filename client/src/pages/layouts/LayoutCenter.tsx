import Header from "../components/Header";


const LayoutCenter = function()
{
    return (
            <section className="page">
                <Header text="Layout Center" />
                <p className="text-xl">Welcome to the Layout Center!</p>
                <hr />
                <br />
                <LayoutSelector />
            </section>
        );
};


export default LayoutCenter;
