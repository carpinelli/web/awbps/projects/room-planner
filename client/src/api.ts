import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";


const BASE_URL = import.meta.env.PROD ? import.meta.env.VITE_API : "";
const roomPlannerApi = createApi({
    reducerPath: "roomPlannerApi",
    baseQuery: fetchBaseQuery({ baseUrl: BASE_URL }),
    endpoints: (builder) => ({
        getAllRooms: builder.query({
            query: () => `rooms?limit=2000`,
        }),
        getRoomsByName: builder.query({
            query: (name: string) => `rooms/${name}`,
        }),
    }),
});


export const { useGetRoomsByName, useGetAllRooms} = roomPlannerApi;
export { roomPlannerApi };
