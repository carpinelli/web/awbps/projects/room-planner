/** @type {import('tailwindcss').Config} */
export default
{
  content:
  [
    "./index.html",
    "./src/**/*.{html,js,ts,jsx,tsx}",
  ],
  darkMode: "class",
  theme:
  {
    extend:
    {
      colors:
      {
        primary:  "#FF7540",  // EXAMPLE.
        secondary: "#254545",  // EXAMPLE.
        subprimary: "#3c5aa6",  // EXAMPLE.
        subsecondary: "#c7a008",  // EXAMPLE.
        pokemonblue: "#2a75bb",  // EXAMPLE.
        pokemonyellow: "#ffcb05",  // EXAMPLE.
        pokemonshadowblue: "#3c5aa6",  // EXAMPLE.
        pokemonshadowyellow: "#c7a008",  // EXAMPLE.
        indigo: "#4B0082",
        darkgray: "#A9A9A9",
        navy: "#000080",  // Add Red.
        darkslategray: "#2F4F4F",  // With darkred.
        darkred: "#8B0000",  // With darkslategray.
        coral: "#FF7F50",  // With darkslategray.
      }
    },
  },
  plugins: [],
};

