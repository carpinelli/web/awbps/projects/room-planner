import type { Request, Response } from "express";
import User from "../models/userModel";


type UserRequest =
{
  request: Request;
  _id: string;
  username: string;
  header: (arg0: string,) => string;
  body: any;
};


const show = async function (request: UserRequest, response: Response)
{
  try
  {
      const foundUser = await User.findById(request._id)
      if (foundUser)
      {
        response.json(
          { 
            username: foundUser.username, 
            email: foundUser.email 
          }
        );
      }
  }
  catch (exception: any)
  {
      response.json({ error: exception.message })
  }
};

export default
{
  show,
};

