// import express from "express";
import type { Request, Response, NextFunction } from "express";
// import mongoose from "mongoose";
// import type { Document, Model } from "mongoose";
import type { Document } from "mongoose";
// import mongoose from "mongoose";

import Rooms from "../models/roomModel";
// import initialRooms from "../models/initialRooms";


type ControllerFunction = (request: Request,
                           response: Response,
                           next?: NextFunction) => Promise<void>;
interface RoomController extends Document
{
  Seed: ControllerFunction;
  Index: ControllerFunction;
  New: ControllerFunction;
  Delete: ControllerFunction;
  Update: ControllerFunction;
  Create: ControllerFunction;
  Edit: ControllerFunction;
  Show: ControllerFunction;
}


const Seed = async function(request: Request, response: Response): Promise<void>
{
  console.log(request);
  await Rooms.deleteMany({});
  // await Rooms.create(initialRooms);
  response.redirect("/rooms");
};

const Index = async function(request: Request, response: Response): Promise<void>
{
  console.log(request);
  const rooms = await Rooms.find().sort({ createdAt: 1});
  response.render("rooms/Index", { rooms });
};


const Delete = async function(request: Request, response: Response): Promise<void>
{
  try
  {
    const room = await Rooms.findByIdAndDelete(request.params._id);
    console.log(room);
    // Handle references here:
  }
  catch (exception: any)
  {
    console.log(exception.message);
    response.status(400).send(exception.message);
  }
  response.redirect("/rooms");
};

const Update = async function(request: Request, response: Response): Promise<void>
{
  await Rooms.findByIdAndUpdate(request.params._id, request.body);
  response.redirect(`/rooms/${request.params._id}`);
};

const Create = async function(request: Request, response: Response): Promise<void>
{
  try
  {
    // request.body.destinations = 
    const newroom = await Rooms.create(request.body);
    console.log(newroom);
    // newroom.destinations = [{ airport: request.body.airport,
                                // arrival: request.body.destinationDate }];
    response.redirect("/rooms");
    // const newroom = new room();
    // // Obtain the default date
    // const dt = newroom.departs;
    // // Format the date for the value attribute of the input
    // const departsDate = dt.toISOString().slice(0, 16);
    // res.render('rooms/new', {departsDate});
  }
  catch (exception: any)
  {
    console.log(exception.message);
    response.status(400).send(exception.message);
  }
};

const Edit = async function(request: Request, response: Response): Promise<void>
{
  const room = await Rooms.findById(request.params._id);
  response.render("rooms/edit", { room });
};

const Show = async function(request: Request, response: Response): Promise<void>
{
  // populate replaces the ids with actual documents/objects we can use
  // const post = await Posts.findById(req.params.id).populate('comments')
  const room = await Rooms.findById(request.params._id).sort({ destinations: 1 });
  response.render("rooms/Show", { room });
};



export default
{
  Seed,
  Index,
  Delete,
  Update,
  Create,
  Edit,
  Show,
} as RoomController;

