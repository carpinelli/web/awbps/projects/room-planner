import type { Request, Response } from "express";
import bcrypt from "bcrypt";
// import jwt from "jsonwebtoken";
const jwt = require("jsonwebtoken");

import User from "../models/userModel";


type UserRequest =
{
  request: Request;
  _id: string;
  username: string;
  header: (arg0: string,) => string;
  body: any;
};


const generateToken = function(user: any)
{
    const payload = { id: user._id, username: user.username };
    const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: "4h" });
    return token;
};

const register = async function (request: UserRequest, response: Response)
{
  try
  {
    const user = await User.findOne({ username: request.body.username });
    // User exists.
    if (user)
    {
        return response.status(400).json({ error: 'User already exists' });
    }

    // User does not exist.
    const encryptedPassword = await bcrypt.hash(request.body.password, Number(process.env.SALT_ROUNDS));
    const newUser = await User.create({ ...request.body, password: encryptedPassword });
    const token = generateToken(newUser);  // Generate a token for the new user.
    response.status(200).json({ token });
  }
  catch(exception: any)
  {
    console.log(exception.message);
    response.status(400).json({ error: exception.message });
  }
};

const login = async function(request: Request, response: Response)
{
  try
  {
    // User exists.
    const user = await User.findOne({ username: request.body.username });
    if (!user) {
        return response.status(404).json({ error: 'No such user exists' });
    }

    // Check if provided password is valid.
    const validPass = await bcrypt.compare(request.body.password, user.password);
    if (!validPass)
    {
      return response.status(400).json({ error: 'Invalid credentials' });
    }

    const token = generateToken(user);
    response.status(200).json({ token });
  }
  catch(exception: any)
  {
    console.log(exception.message);
    response.status(400).json({ error: exception.message });
  }
};


export default
{
    register,
    login
};

