import type { Request, Response } from "express";

import Rooms from "../models/roomModel";
// import initialRooms from "../models/initialRooms";


const Seed = async function(_request: Request, response: Response)
{
  await Rooms.deleteMany({});
  // await Rooms.create(initialRooms);
  response.redirect("/rooms");
};

const Index = async function(_request: Request, response: Response)
{
  const rooms = await Rooms.find().sort({ createdAt: 1});
  response.render("rooms/Index", { rooms });
};


const Delete = async function(request: Request, response: Response)
{
  try
  {
    const room = await Rooms.findByIdAndDelete(request.params._id);
    console.log(room);
    // Handle references here:
  }
  catch (exception: any)
  {
    console.log(exception.message);
    response.status(400).send(exception.message);
  }
  response.redirect("/rooms");
};

const Update = async function(request: Request, response: Response)
{
  try
  {
    await Rooms.findByIdAndUpdate(request.params._id, request.body);
    response.redirect(`/rooms/${request.params._id}`);
  }
  catch (exception: any)
  {
    response.status(400).json({msg: exception.message});
  }
};

const Create = async function(request: Request, response: Response)
{
  try
  {
    // request.body.destinations = 
    const newroom = await Rooms.create(request.body);
    console.log(newroom);
    // newroom.destinations = [{ airport: request.body.airport,
                                // arrival: request.body.destinationDate }];
    response.redirect("/rooms");
    // const newroom = new room();
    // // Obtain the default date
    // const dt = newroom.departs;
    // // Format the date for the value attribute of the input
    // const departsDate = dt.toISOString().slice(0, 16);
    // res.render('rooms/new', {departsDate});
  }
  catch (exception: any)
  {
    console.log(exception.message);
    response.status(400).send(exception.message);
  }
};

const Edit = async function(request: Request, response: Response)
{
  const room = await Rooms.findById(request.params._id);
  response.render("rooms/edit", { room });
};

const Show = async function(request: Request, response: Response)
{
  // populate replaces the ids with actual documents/objects we can use
  // const post = await Posts.findById(req.params.id).populate('comments')
  const room = await Rooms.findById(request.params._id).sort({ destinations: 1 });
  response.render("rooms/Show", { room });
};



export default
{
  Seed,
  Index,
  Delete,
  Update,
  Create,
  Edit,
  Show,
};

