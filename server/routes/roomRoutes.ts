import express from "express";
import type { Router } from "express";

import roomController from "../controllers/roomController";


const router: Router = express.Router();
router.get("/Seed", roomController.Seed);
router.get("/", roomController.Index);
router.delete("/:_id", roomController.Delete);
router.put("/:id/Update", roomController.Update);
router.post("/", roomController.Create);
router.get("/:_id", roomController.Show);


export default router;

