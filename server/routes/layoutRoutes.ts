const express = require("express");

const roomController = require("../controllers/roomController");


const router = express.Router();
router.get("/Seed", roomController.Seed);
router.get("/", roomController.Index);
router.delete("/:_id", roomController.Delete);
router.put("/:id/Update", roomController.Update);
router.post("/", roomController.Create);
router.get("/:_id", roomController.Show);


export default router;

