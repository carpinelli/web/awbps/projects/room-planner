import mongoose from "mongoose";
import type { Schema } from "mongoose";


type Wall =
{
  length: number;
  facingDirection: string;
  order: number;
};
type Door =
{
  length: number;
  facingDirection: string;
  order: number;
};
type Room =
{
  name: string;
  walls: Array<Wall>;
  doors: Array<Door>;
};


const wallSchema: Schema<Wall> = new mongoose.Schema(
  {
    length:
    {
      type: Number,
      required: true,
    },
    facingDirection:
    {
      type: String,
      required: true,
    },
    order:
    {
      type: Number,
      required: true,
    },
  }
);

const doorSchema: Schema<Door> = new mongoose.Schema(
  {
    length:
    {
      type: Number,
      required: true,
    },
    facingDirection:
    {
      type: String,
      required: true,
    },
    order:
    {
      type: Number,
      required: true,
    },
  }
);

const roomSchema: Schema<Room> = new mongoose.Schema(
  {
    name:
    {
      type: String,
      required: true,
      default: Date.toLocaleString()
    },
    walls: [wallSchema],
    doors: [doorSchema],
  },
  { timestamps: true }
);

const Room = mongoose.model("rooms", roomSchema);


export default Room;

