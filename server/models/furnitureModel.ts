import mongoose from "mongoose";
import type
{
  // Document,
  // Model,
  ObjectId,
  Schema,
} from "mongoose";


interface Room
{
  name: string;
  walls: number[];
  doors?: number[];
  _id: ObjectId;
}


const roomSchema: Schema<Room> = new mongoose.Schema<Room>(
  {
    name: {
      type: String,
      required: true,
      default: Date.toLocaleString()
    },
    walls: {
      type: [Number],
      required: true,
      min: 0,
      max: 15,
    },
    doors: {
      type: [Number],
      min: 1,
      max: 5,
    },
  },
  { timestamps: true }
);

const Room = mongoose.model<Room>("rooms", roomSchema);


export default Room;

