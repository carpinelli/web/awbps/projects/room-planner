import express from "express";
import type
{
  Application,
  Request,
  Response,
  NextFunction
}
from "express";
import dotenv from "dotenv";
const jsxEngine = require("jsx-view-engine");
import path from "path";
// const cors = require("cors");

// Ensures environment variables are set before next import statements.
dotenv.config();
import mongoConfig from "./config/mongoDb";
import roomRoutes from "./routes/roomRoutes";


const app: Application = express();
const PORT: number = Number(process.env.PORT);

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jsx");
app.engine("jsx", jsxEngine());
mongoConfig();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(cors);
app.use("/rooms", roomRoutes);

app.use((request: Request, _Response: Response, nextFunction: NextFunction) =>
  {
    console.log(request.path, request.method)
    nextFunction();
  }
);

app.get("/", (_request: Request, response: Response) =>
  {
    try
    {
      response.status(200).json({MSG: "Hi"});
    }
    catch (exception: any)
    {
      console.log(exception.message);
      response.status(400).json({exception: exception.message});
    }
  }
);


app.listen(PORT, () =>
  {
    console.log(`Listenings on http://localhost:${PORT}`);
  }
);

