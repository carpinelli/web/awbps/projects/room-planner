import type { Request, Response, NextFunction, } from "express"
const jwt = require('jsonwebtoken')


type UserRequest =
{
  request: Request;
  _id: string;
  username: string;
  header: (arg0: string,) => string;
};


const authorize = function(request: UserRequest, response: Response, next: NextFunction)
{
  try
  {
    let token = request.header("Authorization");
    if (!token)
    {
      return response.status(403).json({ error: "Missing token."});
    }

    token = token.replace("Bearer ", "");
    const payload = jwt.verify(token, process.env.JWT_SECRET);
    if (payload.error)
    {
      return response.status(403).json({ error: payload.error });
    }

    request._id = payload.id;
    request.username = payload.username;

    next();
  }
  catch (exception: any)
  {
    console.log(exception.message);
    response.status(403).json({ error: exception.message });
  }
}


export default
{
    authorize,
};

