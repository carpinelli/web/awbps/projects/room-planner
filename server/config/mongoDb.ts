const mongoose = require("mongoose");


const mongoConfig = async function()
{
  try
  {
    const response = await mongoose.connect(process.env.MONGO_URL);
    console.log(`Database connected: ${response.connection.host}`);
  }
  catch (exception: any)
  {
    console.log("MongoDB Error: ", exception.message);
  }

  return null;
};


export default mongoConfig;

